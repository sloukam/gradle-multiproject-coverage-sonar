package com.jouda;

import org.junit.Assert;
import org.junit.Test;

public class DeerGroovyTest {

    @Test
    public void runTest() {
        String value = new DeerGroovy().run();
        Assert.assertEquals("deer runs very fast", value);
    }
}
