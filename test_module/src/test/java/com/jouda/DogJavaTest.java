package com.jouda;

import org.junit.Assert;
import org.junit.Test;

public class DogJavaTest {

    @Test
    public void runTest() {
        String value = new DogJava().run();
        Assert.assertEquals("dog is running", value);
    }
}
