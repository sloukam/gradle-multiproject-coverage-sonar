package com.jouda;

import org.junit.Assert;
import org.junit.Test;

public class HorseGroovyTest {

    @Test
    public void runTest() {
        String value = new HorseGroovy().run();
        Assert.assertEquals("horse can run and jump", value);
    }
}
