package com.jouda;

import org.junit.Assert;
import org.junit.Test;

public class ElephantGroovyTest {

    @Test
    public void runTest() {
        String value = new ElephantGroovy().run();
        Assert.assertEquals("elephant can't run too fast", value);
    }
}
