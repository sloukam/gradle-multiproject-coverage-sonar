package com.jouda

import spock.lang.Specification

/**
 * Created by slouka on 03/05/2017.
 */
class LionGroovyTest extends Specification {
    def "Run"() {
        when:
        def value = new LionJava().run()

        then:
        "lion is running" == value
    }
}
