# problem description #

This is repository just to share my project structure. 

* it's a little bit complicated because of it's gradle multiproject witch sources and tests mixed in java and groovy. 
* tests are in same module as source class and sometimes test are in "general" test module (let's call it integration test module) and corresponding sources are located in their module (in this simple case in module *source_module_one* and *source_module_two*.
* some sources are written in java and covered byt groovy test and vice versa.

After spending a lot of time I'm not able to see in my local sonar 100% code coverage. I would really appreciate help to reach target of 100% code coverage. Is it possible with my project structure?