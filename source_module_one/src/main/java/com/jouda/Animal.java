package com.jouda;

/**
 * Created by slouka on 03/05/2017.
 */
@FunctionalInterface
public interface Animal {
    String run();
}
