package com.jouda

import spock.lang.Specification

/**
 * Created by slouka on 03/05/2017.
 */
class CatJavaTest extends Specification {
    def "Run"() {
        when:
        def value = new CatJava().run()

        then:
        "cat is running" == value
    }
}
