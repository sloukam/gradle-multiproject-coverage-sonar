package com.jouda

import spock.lang.Specification

/**
 * Created by slouka on 03/05/2017.
 */
class CowGroovyTest extends Specification {
    def "Run"() {
        when:
        def value = new CowGroovy().run()

        then:
        "cow can't run" == value
    }
}
