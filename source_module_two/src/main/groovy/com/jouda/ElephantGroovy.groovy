package com.jouda

/**
 * Created by slouka on 03/05/2017.
 */
class ElephantGroovy implements Animal {
    @Override
    String run() {
        return "elephant can't run too fast";
    }
}
